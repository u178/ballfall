import cv2
from Vector import Vector
import numpy as np
from skimage.measure import label, regionprops
class Ball:

    def __init__(self, position: Vector, radius) -> None:
        self.position = position
        self.radius = radius
        self.velocity = Vector(0, 0)
        self.bounceness = 0.8

    def check_line_collide(self, image):
        rounded = self.position.round()
        sub_image = image[rounded[1] - self.radius:rounded[1] + self.radius, 
                        rounded[0] - self.radius:rounded[0] + self.radius].copy()
        for i in range(len(sub_image)):
            for j in range(len(sub_image[i])):
                if ((self.radius - i)**2 + (self.radius - j)**2)**(1/2) > self.radius:
                    sub_image[i][j] = 0
        label_img = label(sub_image)
        regions = regionprops(label_img)
        for props in regions:
            if props.area < 5: continue
            y0, x0 = props.centroid
            #self.position += Vector(self.radius/2 - x0, self.radius/2 - y0)
            cv2.circle(sub_image, (round(x0), round(y0)), 3, 255, -1)
            # dir_angle = Vector(x0, y0).argument()
            #
            # vel_angle = self.velocity.argument()
            #
            # r = self.velocity
            # r = r.normalize() * self.radius
            # cv2.line(sub_image, (self.radius, self.radius), (int(r[0]) + self.radius, int(r[1]) + self.radius), 255, 1)
            #
            # next_vel_angle = vel_angle + (dir_angle - vel_angle)
            #
            # r = self.velocity.rotate(next_vel_angle)
            # r = r.normalize() * self.radius
            # cv2.line(sub_image, (self.radius, self.radius), (int(r[0]) + self.radius, int(r[1]) + self.radius), 255, 1)
            #
            # self.velocity = self.velocity.rotate(next_vel_angle)
            # if self.velocity.length() > 10:
            #     self.velocity.vector_mul(Vector(self.bounceness, self.bounceness))
            a = (Vector(-x0, -y0).normalize() - self.velocity.normalize()).normalize()
            self.velocity = a * self.velocity.length()
            self.velocity.vector_mul(Vector(self.bounceness, self.bounceness))
            # a -= self.velocity.normalize()
            # a = a.normalize()
            # 

        #cv2.imshow("Ball", sub_image)

    def check_wall_collide(self, width, height):
        if self.position[0] - self.radius < 0 or self.position[0] + self.radius > width:
            self.position = Vector(self.radius if self.position[0] - self.radius <= 0 else width - self.radius, self.position[1]) 
            self.velocity.vector_mul(Vector(-self.bounceness, 1))
        if self.position[1] - self.radius < 0 or self.position[1] + self.radius > height:
            #print(-0.5 < self.velocity[0] < 0.5)
            #self.velocity.vector_mul(Vector(1 if -0.5 < self.velocity[0] < 0.5 else self.bounceness, -self.bounceness))
            self.velocity.vector_mul(Vector(1, -self.bounceness))
            self.position = Vector(self.position[0], self.radius if self.position[1] - self.radius <= 0 else height - self.radius)
    
    def check_ball_collide(self, balls):
        for ball in balls:
            if ball.position == self.position: continue
            if ((ball.position[0] - self.position[0]) ** 2 + (ball.position[1] - self.position[1]) ** 2) <= (self.radius + ball.radius) ** 2:
                delta = self.position - ball.position
                d = delta.length()
                mtd = delta * (((self.radius + ball.radius)-d)/d)
                self.position += mtd * 0.5
                ball.position -= mtd * 0.5
                vel = self.velocity - ball.velocity
                vn = mtd.normalize() * vel
                if (vn > 0): continue
                impulse = mtd.normalize() * -vn
                self.velocity += impulse
                ball.velocity += impulse
                #self.velocity.vector_mul((self.velocity.normalize() - ball.velocity.normalize()).normalize())
                #self.velocity -= ball.velocity
                #self.velocity.vector_mul(Vector(self.bounceness, self.bounceness))

    def check_collide(self, width, height, image, balls):
        self.check_wall_collide(width, height)
        # self.check_ball_collide(balls)
        # for ball in balls:
        #     rounded = ball.position.round()
        #     for i in range(rounded[0www] - ball.radius,rounded[0] + ball.radius+1):
        #         for j in range(rounded[1] - ball.radius,rounded[1] + ball.radius+1):
        #             if ((ball.radius - i)**2 + (ball.radius - j)**2) <= ball.radius ** 2:
        #                 image[i][j] = 0
        self.check_line_collide(image)
            
    def update(self, width, height, image, balls, delta):
        im = image.copy()
        #print(self.position)
        self.add_force(Vector(0, 1), delta)
        self.check_collide(width, height, im, balls)#, balls)
        self.position += self.velocity


    def add_force(self, force, delta):
        self.velocity += force * delta
        #print(self.velocity)

    def draw(self, image):
        cv2.circle(image, self.position.round().values, self.radius, (0, 0, 255), -1)
import cv2
import numpy as np
import matplotlib.pyplot as plt
import loguru
import time
from Ball import Ball
from Vector import Vector

cap = cv2.VideoCapture(2)
# cap = cv2.VideoCapture('data/video2.mp4')

points_array = []

# test
points_array = [(179, 55), (132, 330), (523, 384), (534, 101)]
balls = [Ball(Vector(200, 200), 10) for i in range(1)]
delta = time.perf_counter()


def get_warp_matrix(points):
    # this function sets global variables, be careful!
    global maxWidth, maxHeight
    width_AD = np.sqrt(((points[0][0] - points[3][0]) ** 2) + ((points[0][1] - points[3][1]) ** 2))
    width_BC = np.sqrt(((points[1][0] - points[2][0]) ** 2) + ((points[1][1] - points[2][1]) ** 2))
    maxWidth = max(int(width_AD), int(width_BC))

    height_AB = np.sqrt(((points[0][0] - points[1][0]) ** 2) + ((points[0][1] - points[1][1]) ** 2))
    height_CD = np.sqrt(((points[2][0] - points[3][0]) ** 2) + ((points[2][1] - points[3][1]) ** 2))
    maxHeight = max(int(height_AB), int(height_CD))

    input_pts = np.float32([points[0], points[1], points[2], points[3]])
    output_pts = np.float32([[0, 0],
                             [0, maxHeight - 1],
                             [maxWidth - 1, maxHeight - 1],
                             [maxWidth - 1, 0]])
    return cv2.getPerspectiveTransform(input_pts, output_pts)


def binarize_image(img, blur=False):
    # img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img_gray = img

    if blur:
        img_gray = cv2.GaussianBlur(img_gray, (7, 7), 0)

    ret, mask = cv2.threshold(img_gray, 140, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)

    th3 = cv2.adaptiveThreshold(img_gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, \
                               cv2.THRESH_BINARY, 11, 2)

    # masked = cv2.bitwise_and(img_gray, img_gray, mask=mask)
    masked = remove_salt_pepper(th3)
    return masked


def remove_salt_pepper(img):
    median_blur = cv2.medianBlur(img.copy(), 5)
    # median_blur = cv2.erode(median_blur.copy(), np.ones((3, 3), 'uint8'), iterations=1)
    # median_blur = cv2.medianBlur(median_blur, 3)
    # median_blur = cv2.medianBlur(median_blur, 3)

    # median_blur = cv2.dilate(median_blur, np.ones((3, 3), 'uint8'), iterations=1)
    return median_blur


def click_event(event, x, y, flags, params):
    # checking for left mouse clicks
    if event == cv2.EVENT_LBUTTONDOWN:
        points_array.append((x, y))
        print(points_array)


def binarize_blue(img):
    # print(img.shape)

    #ToDo remove red
    img_hsv = cv2.cvtColor(img.copy(), cv2.COLOR_BGR2HSV)
    lower_red = np.array([50, 50, 140])
    upper_red = np.array([255, 255, 200])
    mask = cv2.inRange(img_hsv, lower_red, upper_red)
    img_hsv[np.where(mask)] = (255, 255, 255)

    img_bgr = cv2.cvtColor(img_hsv.copy(), cv2.COLOR_HSV2BGR)

    last_img = img_bgr[:, :, 0]
    #print(last_img.shape)

    last_img[np.where(last_img < 170)] = 255
    last_img[-15:, :] = 255
    cv2.imshow("last", last_img)

    return binarize_image(last_img)


def draw_contours(bin_image, original_img):

    contours, hierarchy = cv2.findContours(bin_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    cv2.drawContours(original_img, contours, -1, (0, 255, 0), 3)
    # cv2.dilate(original_img, np.ones((3, 3), 'uint8'), iterations=1)



try:
    ret, frame = cap.read()
    cv2.imshow("RegionSelection", frame)
    cv2.setMouseCallback('RegionSelection', click_event)
    # print(frame.shape)
except Exception as inst:
    print("video capture problems")
    print(type(inst))
    print(inst.args)
    print(inst)

while ret:
    if cv2.waitKey(25) & 0xFF == ord('g'):
        if len(points_array) == 4:
            print("go next")
            cv2.destroyWindow("RegionSelection")
            warp_matrix = get_warp_matrix(points_array)
            warped_image = cv2.warpPerspective(frame, warp_matrix, (maxWidth, maxHeight), flags=cv2.INTER_LINEAR)
            break
        elif len(points_array) > 4:
            print("select 4 points again")
            points_array.clear()
        else:
            print(f"4 points required. Current points: {len(points_array)}")

    if cv2.waitKey(25) & 0xFF == ord('r'):
        print("clear")
        points_array.clear()


# cv2.imshow('WarpedImage', warped_image)

set_up_flag = True
first_run = True
while cap.isOpened():
    start = time.perf_counter()

    if set_up_flag:
        if first_run:
            ret, frame = cap.read()
            first_run = False
            im = cv2.warpPerspective(frame, warp_matrix, (maxWidth, maxHeight), flags=cv2.INTER_LINEAR)
            bin_im = binarize_blue(im)
    else:
        ret, frame = cap.read()

    if not ret:
        break

    cv2.imshow('Frame', frame)



    # bin_im = binarize_image(im)
    # print(f"shape: {im.shape}")

    # print(f"type: {type(im)}")
    ball_img = np.ones((bin_im.shape[0], bin_im.shape[1], 3))
    #ball_img[30:40, 100:200] = 255
    print(f"bin_img shp: {bin_im.shape}")
    print(ball_img.shape)

    if not set_up_flag:

        for ball in balls:
            ball.update(bin_im.shape[1], bin_im.shape[0], bin_im, balls, delta)
            ball.draw(ball_img)
        delta = time.perf_counter() - start
    cv2.imshow('warped', bin_im)

    if set_up_flag:
        contoured_img = im.copy()
        draw_contours(bin_im, contoured_img)
        cv2.imshow('contoured', contoured_img)
        resized_contoured_img = cv2.resize(contoured_img, (900, 650), interpolation=cv2.INTER_AREA)
        cv2.imshow('GAME', resized_contoured_img)
    else:
        resized_ball_img = cv2.resize(ball_img, (900, 650), interpolation=cv2.INTER_AREA)
        cv2.imshow('GAME', resized_ball_img)

    if cv2.waitKey(25) & 0xFF == ord('p'):
        set_up_flag = not set_up_flag
        if set_up_flag:
            first_run = True
        print(f"set up: {set_up_flag}")

    key = cv2.waitKey(1)
    if key == ord('w'):
        for ball in balls:
            ball.add_force(Vector(0, -1), 1)
    if key == ord('d'):
        for ball in balls:
            ball.add_force(Vector(1, 0), 1)
    if key == ord('a'):
        for ball in balls:
            ball.add_force(Vector(-1, 0), 1)
    if key == ord('s'):
        for ball in balls:
            ball.add_force(Vector(0, 1), 1)

    if key & 0xFF == ord('q'):
        break

    # print(type(bin_im))
    # print(bin_im.shape)
    # print(type(im))
    # print(im.shape)
    # with open("data/bin_img2.npy", 'wb') as f:
    #     np.save(f, bin_im)
    #
    # with open("data/im2.npy", 'wb') as g:
    #     np.save(g, im)
    # break

    # plt.figure("131")
    # plt.imshow(im)
    # plt.figure("132")
    # plt.imshow(bin_im)
    # plt.figure("133")
    # plt.imshow(contoured_img)
    # plt.show()
    # break

    # Press Q on keyboard to  exit


while True:
    if cv2.waitKey(25) & 0xFF == ord('q'):
        break

cap.release()

cv2.destroyAllWindows()

